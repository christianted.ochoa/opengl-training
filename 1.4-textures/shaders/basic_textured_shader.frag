#version 330 core

uniform sampler2D mainTex;

in vec3 vertexColor;
in vec2 texCoord;

out vec4 FragColor;

void main()
{
	FragColor = texture(mainTex, texCoord) * vec4(vertexColor, 1.0f);
}