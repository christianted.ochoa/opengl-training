#version 330 core

uniform sampler2D mainTex;
uniform sampler2D overlayTex;

in vec3 vertexColor;
in vec2 texCoord;

out vec4 FragColor;

void main()
{
	FragColor = mix(texture(mainTex, texCoord), texture(overlayTex, texCoord * vec2(-1, 1)), 0.2);
}