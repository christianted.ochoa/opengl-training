#version 330 core

uniform float hOffset;

layout (location = 0) in vec3 aPos;

void main()
{
	gl_Position = vec4(aPos.x + hOffset, aPos.y, aPos.z, 1.0f);
}