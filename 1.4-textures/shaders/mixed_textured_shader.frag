#version 330 core

uniform sampler2D mainTex;
uniform sampler2D overlayTex;
uniform float mixAmount;

in vec3 vertexColor;
in vec2 texCoord;

out vec4 FragColor;

void main()
{
	FragColor = mix(texture(mainTex, texCoord), texture(overlayTex, texCoord), mixAmount);
}